
# Import module
from random import shuffle

# Set range and shuffle 
num = [[i] for i in range(1, 11)]
shuffle(num)

# Print results
print(num)
