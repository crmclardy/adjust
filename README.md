# adjust
**Introduction**

This is a small python script to print randomly the numbers from 1-10.
I don't actually know python yet but I chose it for this task as it's a language I want to get a handle on and use more frequently.
I already had an idea of what to do here in bash it didn't seem like it would be hard to switch over.

**Pre-Requisites**

OSX:
OSX has Python 2.7 pre-installed which will be enough for our needs.

Linux:
Install python using 'sudo yum install python36' for Red Hat systems or 'sudo apt-get install python3.6' for Ubuntu systems.

**Execute:**

To execute the random number script, navigate in your terminal into the *adjust* folder with 
`cd adjust`,
then run the following command:
`python numbers.py`

